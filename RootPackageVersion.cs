﻿using System.Collections.Generic;

namespace PackageTestSource2
{
    public class RootPackageVersion
    {
        public const string VERSION = "0.2.2-vtest";

        /// <summary>
        /// Sums the values in the input collection
        /// </summary>
        /// <param name="values">Numbers to sum</param>
        /// <returns>The sum</returns>
        public static int Sum(IEnumerable<int> values)
        {
            int sum = 0;
            foreach (var value in values)
            {
                sum += value;
            }
            
            return sum;
        }
    }
}